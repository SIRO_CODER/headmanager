package com.gitlab.sirocoder.headmanager.Utils;

import org.bukkit.FluidCollisionMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.util.RayTraceResult;
import java.util.function.Predicate;

public final class Heads {

    public static boolean IsHead(Material material)
    {
        return material.equals(Material.PLAYER_HEAD) ||
                material.equals(Material.PLAYER_WALL_HEAD);
    }

    public static boolean ProhibitedActions(InventoryAction Action)
    {
        return  Action.equals(InventoryAction.PICKUP_ALL) ||
                Action.equals(InventoryAction.PICKUP_HALF) ||
                Action.equals(InventoryAction.PICKUP_ONE) ||
                Action.equals(InventoryAction.PICKUP_SOME)||
                Action.equals(InventoryAction.MOVE_TO_OTHER_INVENTORY);
    }

    public static RayTraceResult RayTraceEntity(Player player, double Range, EntityType entityType)
    {
        World world = player.getWorld();
        Location eyeLoc = player.getEyeLocation();
        Predicate<Entity> filter = ent -> ent.getType().equals(entityType);
        return world.rayTrace(eyeLoc, eyeLoc.getDirection(), Range, FluidCollisionMode.NEVER, true, 1, filter);
    }

    public static Block TargetBlocks(Player player, int Range)
    {
        return player.getTargetBlockExact(Range, FluidCollisionMode.ALWAYS);
    }
}
