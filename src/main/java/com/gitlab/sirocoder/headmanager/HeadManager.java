package com.gitlab.sirocoder.headmanager;

import com.gitlab.sirocoder.headmanager.Events.*;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

public final class HeadManager extends JavaPlugin {

    @Override
    public void onEnable() {
        // Plugin startup logic
        //イベントの登録
        RegisterEvents();
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        HandlerList.unregisterAll(this);
    }

    public void RegisterEvents()
    {
        new AntiHeadDrop(this);
        new AntiHeadPickup(this);
        new AntiHopper(this);
        new AntiInventoryMove(this);
        new AntiArmorStand(this);
        new AntiHeadCopy(this);
    }
}
