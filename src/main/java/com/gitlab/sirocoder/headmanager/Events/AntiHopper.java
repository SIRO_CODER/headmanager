package com.gitlab.sirocoder.headmanager.Events;

import com.gitlab.sirocoder.headmanager.Utils.Heads;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryPickupItemEvent;
import org.bukkit.plugin.Plugin;

public class AntiHopper implements Listener {

    public AntiHopper(Plugin plugin)
    {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void AntiHopper(InventoryPickupItemEvent e)
    {
        if(Heads.IsHead(e.getItem().getItemStack().getType())) e.setCancelled(true);
    }
}
