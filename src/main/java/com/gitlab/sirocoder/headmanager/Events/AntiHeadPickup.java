package com.gitlab.sirocoder.headmanager.Events;

import com.gitlab.sirocoder.headmanager.Utils.Heads;
import jdk.javadoc.internal.doclets.formats.html.markup.Head;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.plugin.Plugin;

public class AntiHeadPickup implements Listener {

    public AntiHeadPickup(Plugin plugin)
    {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void AntiEntityPickup(EntityPickupItemEvent e)
    {
        Material pickupItem = e.getItem().getItemStack().getType();
        if(Heads.IsHead(pickupItem))
            e.setCancelled(true);
    }
}
