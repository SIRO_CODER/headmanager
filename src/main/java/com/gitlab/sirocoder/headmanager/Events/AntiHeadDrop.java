package com.gitlab.sirocoder.headmanager.Events;

import com.gitlab.sirocoder.headmanager.Utils.Heads;
import net.md_5.bungee.api.chat.hover.content.Item;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;

public class AntiHeadDrop implements Listener {

    public AntiHeadDrop(Plugin plugin)
    {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void AntiDrop(PlayerDropItemEvent e)
    {
       ItemStack item =  e.getItemDrop().getItemStack();
       if(Heads.IsHead(item.getType()))
           e.setCancelled(true);
    }
}
