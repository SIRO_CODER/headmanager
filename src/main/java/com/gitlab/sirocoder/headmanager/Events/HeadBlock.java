package com.gitlab.sirocoder.headmanager.Events;

import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.RayTraceResult;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.function.Predicate;

public class HeadBlock implements Listener {
    //分割中
    private final Plugin plugin;
    private final ArrayList<InventoryType> invTypeList;

    public HeadBlock(Plugin plugin){
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        this.plugin = plugin;
        invTypeList = new ArrayList<>();
        AddinvList();
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void AntiHeadCopyEntity(InventoryCreativeEvent e)
    {
        Player player = (Player) e.getWhoClicked();
        Entity LineOfSightEntity = EntityRayTrace(player, 6).getHitEntity();
        ItemStack currentItem = e.getCursor();

        if(LineOfSightEntity == null) return;

        if(LineOfSightEntity.getType() == EntityType.ITEM_FRAME)
        {
            plugin.getServer().broadcast(player.getName() + "がヘッドをコピーしようとしました", "op");
            player.sendMessage(ChatColor.RED + "プレイヤーヘッドはコピーできません");
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void AntiHeadCopyBlock(InventoryCreativeEvent e)
    {
        Player player = (Player) e.getWhoClicked();
        Block  LineOfSightBlock = EntityRayTrace(player, 6).getHitBlock();

        ItemStack oldItem = e.getCursor();
        ItemStack currentItem = e.getCurrentItem();
        Inventory inv = player.getInventory();
        if(LineOfSightBlock == null) return;


        if(isHead(LineOfSightBlock.getType()) && isHead(oldItem.getType()) || isHead(currentItem.getType()))
        {
            plugin.getServer().broadcast(player.getName() + "がヘッドをコピーしようとしました", "op");
            player.sendMessage(ChatColor.RED + "プレイヤーヘッドはコピーできません");
            e.setCancelled(true);
        }
    }

    /*
    @EventHandler
    public void AntiHeadDrop(PlayerDropItemEvent e)
    {
        ItemStack currentItem = e.getItemDrop().getItemStack();
        if(isHead(currentItem.getType()))
        {
            e.setCancelled(true);
            currentItem.setAmount(currentItem.getAmount());
        }
    }
    */


    @EventHandler
    public void AntiMoveHead(InventoryMoveItemEvent e)
    {
        if(isHead(e.getItem().getType())) e.setCancelled(true);
    }

    /*
    @EventHandler
    public void AntiHopper(InventoryPickupItemEvent e)
    {
        if(isHead(e.getItem().getItemStack().getType())) e.setCancelled(true);
    }
    */

    /*
    @EventHandler
    public void AntiPickupHead(EntityPickupItemEvent e)
    {
        if(isHead(e.getItem().getItemStack().getType())) e.setCancelled(true);
    }
     */

    @EventHandler
    public void AntiInvMove(InventoryClickEvent e)
    {
        InventoryType inventoryType = e.getInventory().getType();
        InventoryAction action = e.getAction();
        ItemStack currentItem = e.getCurrentItem();
        if(!isHead(currentItem.getType())) return;

        for(InventoryType IT : invTypeList)
        {
            if(inventoryType == IT && AntiItemMove(action) && isHead(currentItem.getType()))
            {
                e.setCancelled(true);
            }
        }
    }
    /*
    @EventHandler
    public void AntiArmorStand(PlayerArmorStandManipulateEvent e)
    {
        ItemStack currentItem = e.getArmorStandItem();
        if(isHead(currentItem.getType())) e.setCancelled(true);
    }
    */

    private boolean isHead(Material material)
    {
        if(material == Material.PLAYER_HEAD)
            return true;
        if(material == Material.PLAYER_WALL_HEAD)
            return true;

        return false;
    }

    private boolean AntiItemMove(InventoryAction Action)
    {
        if(Action == InventoryAction.PICKUP_ALL)
            return true;
        else if (Action == InventoryAction.PICKUP_HALF)
            return true;
        else if (Action == InventoryAction.PICKUP_ONE)
            return true;
        else if (Action == InventoryAction.PICKUP_SOME)
            return true;
        else if(Action == InventoryAction.MOVE_TO_OTHER_INVENTORY)
            return true;
        else
            return false;
    }

    private RayTraceResult EntityRayTrace(Player player, double MaxRange)
    {
        World world = player.getWorld();
        Vector Direction  = player.getEyeLocation().getDirection();
        Location eyeLoc = player.getEyeLocation();

        Predicate<Entity> filter = ent -> ent.getType() == EntityType.ITEM_FRAME;

        RayTraceResult rayTraceResult = world.rayTrace(eyeLoc, Direction, MaxRange, FluidCollisionMode.NEVER, true, 0.5, filter);
        if(rayTraceResult == null) return null;

        return rayTraceResult;
    }

    private void AddinvList()
    {
        invTypeList.add(InventoryType.ANVIL);
        invTypeList.add(InventoryType.FURNACE);
        invTypeList.add(InventoryType.GRINDSTONE);
        invTypeList.add(InventoryType.BLAST_FURNACE);
        invTypeList.add(InventoryType.BARREL);
        invTypeList.add(InventoryType.BEACON);
        invTypeList.add(InventoryType.BREWING);
        invTypeList.add(InventoryType.CHEST);
        invTypeList.add(InventoryType.DISPENSER);
        invTypeList.add(InventoryType.DROPPER);
        invTypeList.add(InventoryType.ENCHANTING);
        invTypeList.add(InventoryType.CRAFTING);
        invTypeList.add(InventoryType.ENDER_CHEST);
        invTypeList.add(InventoryType.HOPPER);
        invTypeList.add(InventoryType.SHULKER_BOX);
        invTypeList.add(InventoryType.STONECUTTER);
        invTypeList.add(InventoryType.SMOKER);
        invTypeList.add(InventoryType.SMITHING);
    }
}
