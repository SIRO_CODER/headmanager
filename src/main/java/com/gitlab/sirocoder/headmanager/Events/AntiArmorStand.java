package com.gitlab.sirocoder.headmanager.Events;

import com.gitlab.sirocoder.headmanager.Utils.Heads;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

public class AntiArmorStand implements Listener {

    public AntiArmorStand(Plugin plugin)
    {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void AntiArmorHead(PlayerArmorStandManipulateEvent e)
    {
        ItemStack item = e.getArmorStandItem();
        if(Heads.IsHead(item.getType())) e.setCancelled(true);
    }
}
